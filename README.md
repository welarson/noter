Noter
====

Note
----
This project is still in development and is incomplete.

Overview
--------
Noter is a simple example application to demonstrate how to create a web application with a PHP REST backend
combined with a single-page front-end using Angular2. The application simply lets a registered user create
notes with categories. Authentication will utilize JWT.

Objective
---------
This example is intended to help someone getting started creating web applications to get up and going. It could also be used as a skeleton to build a web application around. Since I don't know the knowledge level of any potential users, I'll keep things pretty basic so bear with me if I go over stuff you already know.

I'm using Linux so the instructions will be orientated toward Linux, but it should work pretty much the same for OS/X. I'm not so sure about how things work on Windows.

Tools
-----
We'll be using an assortment of tools to create this project. The version numbers are the versions I used and often earlier versions will work okay.

Apache   2.4  - Web Server (https://httpd.apache.org/)  
MySQL    5.6+ - Database (https://www.mysql.com/)  
PHP      5.6+ - Web Server-Side Scripting (http://php.net/)  
Node.js  5.7+ - Javascript Runtime (https://nodejs.org)  
Composer 1.0.0a10 - PHP dependency manager (https://getcomposer.org/)

There are also a number of libraries we'll be needing, but you don't need to worry about getting and installing them, because Node.js and Composer will take care of it for you.


Getting Started
---------------
### Getting the Code
The first thing you need to do to get Noter running is to get the code. For that you will need git. If you aren't familiar with git, then go check out the git website at (https://git-scm.com). It will hook you up with the software and provide documentation.

Once git is ready, you'll want to go to the directory where you want to create the project directory. Git will create a directory named 'noter' there when you get the code. In my case, I'll put the project in a folder named 'git' in my home directory.
```
cd /home/erik/git
```

To clone the project, you'll need to use the following git command.
```
git clone git@gitlab.com:welarson/noter.git
```

This will create the noter directory and copy all the source code into it.

### Downloading dependencies
...

### Setting Up Apache and PHP
...

### Setting Up MySQL
...

### Configuration
...


Using Noter
-----------
...

How it Works
------------
...

Creating Your Own Application
-----------------------------
...
