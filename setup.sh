#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )"

echo Setting Up Backend
cd $DIR/back
composer update

if [ $? -eq 0 ]; then
   echo Composer Finished
else
   echo Composer Failed
   exit 1
fi

echo Building SQL...
vendor/bin/propel sql:build
echo Executing SQL...
vendor/bin/propel sql:insert
echo Building Models...
vendor/bin/propel model:build
echo Converting Configuration...
vendor/bin/propel config:convert
