<?php
require_once dirname(__FILE__) . '/../bootstrap.php';

$app->get('/', function($request, $response, $args) {
   return $response->write("Welcome to the Noter backend!");
});

// These routes do not require authentication.
$app->post('/login', 'Noter\UserController:login');
$app->post('/register', 'Noter\UserController:register');

// This route does not require session management.
$app->group('/user', function() {
   $this->get('/logout', 'Noter\UserController:logout');
})->add($jwtMiddleware);

// These routes require authentication.
$app->group('/api', function() {
   // Get list of categories for the user.
   $this->get('/categories', 'Noter\CategoryController:lister');
   // Get a specific category.
   $this->get('/category/{id}', 'Noter\CategoryController:get');
   // Create a new category for the user.
   $this->post('/category', 'Noter\CategoryController:create');
   // Update an existing category for the user.
   $this->put('/category/{id}', 'Noter\CategoryController:update');
   // Delete a category for the user.
   $this->delete('/category/{id}', 'Noter\CategoryController:delete');

   // Get list of notes (query parameters will be used to provide options)
   $this->get('/notes', 'Noter\NoteController:lister');
   // Get a specific note.
   $this->get('/note/{id}', 'Noter\NoteController:get');
   // Create a new note
   $this->post('/note', 'Noter\NoteController:create');
   // Update a note.
   $this->put('/note/{id}', 'Noter\NoteController:update');
   // Delete a note.
   $this->delete('/note/{id}', 'Noter\NoteController:delete');

})->add($sessionMiddleware)->add($jwtMiddleware);

$app->run();
?>
