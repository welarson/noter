<?php
require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once dirname(__FILE__) . '/generated-conf/config.php';

use Cerberus\JWTAuthMiddleware;
use Noter\Constants;
use Noter\SessionMiddleware;

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$container = new \Slim\Container($configuration);

$app = new \Slim\App($container);

$container['Noter\UserController'] = function($c) {
   return new Noter\UserController();
};
$container['Noter\CategoryController'] = function($c) {
   return new Noter\CategoryController();
};

// Authentication is taken care of by this middleware
$jwtMiddleware = new JWTAuthMiddleware([
   JWTAuthMiddleware::HTTPS_REQUIRED      => false,
   JWTAuthMiddleware::SECRET              => Constants::JWT_SECRET,
   JWTAuthMiddleware::JWT_DATA_ATTRIBUTE  => Constants::JWT_ATTR
]);

$sessionMiddleware = new SessionMiddleware();
?>
