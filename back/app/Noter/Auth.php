<?php
namespace Noter;

use Cerberus\JWTManager;

class Auth {

   public static function createJWTToken($token, $userId) {
      $jwtToken = JWTManager::createToken($token,
         Constants::SERVER_NAME,
         Constants::TOKEN_EXPIRE_TIME,
         Constants::JWT_SECRET,
         [ Constants::USER_ID => $userId]
      );
      return $jwtToken;
   }

}
?>
