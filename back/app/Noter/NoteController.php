<?php
namespace Noter;

use Propel\Runtime\Map\TableMap;
use NoterData\Note;
use NoterData\NoteQuery;

class NoteController {
   public function lister($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      $notes = NoteQuery::create()->filterByUserId($userId)->orderByTitle()->find();
      if ($notes === null) {
         return HTTP::error($response, 500, 'Error getting notes.');
      }
      $data = [];
      foreach ($notes as $note) {
         $data[] = $note->asMinimalArray();
      }

      return HTTP::jsonResponse($response, $body = [ 'notes' => $data]);
   }

   public function get($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      // Be sure to include the user id in the query so the note of another user can't be retrieved.
      $note = NoteQuery::create()->filterById($args['id'])->filterByUserId($userId)->findOne();
      if ($note === null) {
         return HTTP::error($response, 404, 'Note not found');
      }

      return HTTP::jsonResponse($response, $body = [ 'note' => $note->asMinimalArray()]);
   }

   public function create($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;
      $values = $request->getParsedBody();

      $existingnote = NoteQuery::create()->filterByName($values['name'])->findOne();
      if ($existingnote !== null) {
         return HTTP::error($response, 400, 'Note already exists');
      }

      $note = new Note();
      $note->setUserId($userId);
      $note->fromArray($values, TableMap::TYPE_COLNAME);
      $note->save();

      return HTTP::jsonResponse($response, $body = [ 'error' => 'none' ]);

   }

   public function update($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      // Be sure to include the user id in the query so the note of another user can't be retrieved.
      $note = NoteQuery::create()->filterById($args['id'])->filterByUserId($userId)->findOne();
      if ($note === null) {
         return HTTP::error($response, 404, 'note not found');
      }

      $note->fromArray($request->getParsedBody(), TableMap::TYPE_COLNAME);
      $note->save();

      return HTTP::jsonResponse($responser, $body = [ 'error' => 'none' ]);
   }

   public function delete($resquest, $responser, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;
      // Be sure to include the user id.
      NoteQuery::create()->filterById($args['id'])->filterByUserId($userId)->delete();

      return HTTP::jsonResponse($responser, $body = [ 'error' => 'none' ]);
   }
}
?>
