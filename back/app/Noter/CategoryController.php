<?php
namespace Noter;

use Propel\Runtime\Map\TableMap;
use NoterData\Category;
use NoterData\CategoryQuery;

class CategoryController {

   public function lister($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      $categories = CategoryQuery::create()->filterByUserId($userId)->orderByName()->find();
      if ($categories === null) {
         return HTTP::error($response, 500, 'Error getting categories.');
      }
      $data = [];
      foreach ($categories as $category) {
         $data[] = $category->asMinimalArray();
      }

      return HTTP::jsonResponse($response, $body = [ 'categories' => $data]);
   }

   public function get($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      // Be sure to include the user id in the query so the category of another user can't be retrieved.
      $category = CategoryQuery::create()->filterById($args['id'])->filterByUserId($userId)->findOne();
      if ($category === null) {
         return HTTP::error($response, 404, 'Category not found');
      }

      return HTTP::jsonResponse($response, $body = [ 'category' => $category->asMinimalArray()]);
   }

   public function create($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;
      $values = $request->getParsedBody();

      $existingCategory = CategoryQuery::create()->filterByName($values['name'])->findOne();
      if ($existingCategory !== null) {
         return HTTP::error($response, 400, 'Category already exists');
      }

      $category = new Category();
      $category->setUserId($userId);
      $category->fromArray($values, TableMap::TYPE_COLNAME);
      $category->save();

      return HTTP::jsonResponse($response, $body = [ 'error' => 'none' ]);

   }

   public function update($request, $response, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;

      // Be sure to include the user id in the query so the category of another user can't be retrieved.
      $category = CategoryQuery::create()->filterById($args['id'])->filterByUserId($userId)->findOne();
      if ($category === null) {
         return HTTP::error($response, 404, 'Category not found');
      }

      $category->fromArray($request->getParsedBody(), TableMap::TYPE_COLNAME);
      $category->save();

      return HTTP::jsonResponse($responser, $body = [ 'error' => 'none' ]);
   }

   public function delete($resquest, $responser, $args) {
      $userId = $request->getAttribute(Constants::JWT_ATTR)->user;
      // Be sure to include the user id.
      CategoryQuery::create()->filterById($args['id'])->filterByUserId($userId)->delete();

      return HTTP::jsonResponse($responser, $body = [ 'error' => 'none' ]);
   }
}
?>
