<?php
namespace Noter;

class HTTP {
   public static function jsonResponse($response, $data) {
      return $response->withHeader('Content-type', 'application/json')
                      ->write(json_encode($data));
   }

   public static function error($response, $status, $msg) {
      return self::jsonResponse($response->withStatus($status), ['msg' => $msg]);
   }
}
?>
