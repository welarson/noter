<?php
namespace Noter;

class Constants {
   // Authentication tokens are good for three days.
   const TOKEN_EXPIRE_TIME = 60*60*24*3;

   // This is the secret that is used to sign JWT tokens.
   const JWT_SECRET = "ThisIsALongSuperSecrektStringThatShouldProbablyBeInTheEnvironment";

   // This is the name of the request attribute the decoded JWT data is placed in.
   const JWT_ATTR = "jwt-data";

   // Server Name
   const SERVER_NAME = "api.noter.local";

   // User Id attribute
   const USER_ID = "user";

   // Helper constant for the JWT token attribute.
   const JWT_TOKEN_ATTRIBUTE  = "jti";
}

?>
