<?php
namespace Noter;

use NoterData\SessionQuery;

class SessionMiddleware {
   public function __invoke($request, $response, callable $next) {

      $session_token = $request->getAttribute(Constants::JWT_ATTR)->jti;
      if (!$session_token) {
         return HTTP::error($response, 401, 'Missing Session Token');
      }

      $session = SessionQuery::create()->findPK($session_token);
      if (!$session) {
         return HTTP::error($response, 401, 'Invalid Session Token');
      }

      if ($session->getExpire()->getTimestamp() < time()) {
         return HTTP::error($response, 401, 'Session Expired');
      }

      $response = $next($request, $response);

      $current_time = time();
      $session->setAccessed($current_time);
      $session->setExpire($current_time + Constants::TOKEN_EXPIRE_TIME);
      $session->save();

      $user_id = $request->getAttribute(Constants::JWT_ATTR)->user;

      $jwtToken = Auth::createJWTToken($session->getId(), $user_id);

      return $response->withHeader('X-JWT', $jwtToken);
   }
}

?>
