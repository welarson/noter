<?php
namespace Noter;

use Cerberus\JWTManager;
use NoterData\User;
use NoterData\UserQuery;
use NoterData\Session;

class UserController {
   public function login($request, $response, $args) {
      $values = $request->getParsedBody();

      $user = UserQuery::create()->findOneByEmail($values['email']);
      if ($user != null) {
         if ( password_verify($values['password'], $user->getPassword())) {
            return $this->createLoginResponse($response, $user);
         }
      }

      return HTTP::error($response, 401, 'User and password combination not valid.');
   }

   public function register($request, $response, $args) {
      $values = $request->getParsedBody();

      //TODO: Obviously the email needs to be checked.
      $newUser = new User();
      $newUser->setEmail($values['email']);
      // Note: The password_hash function includes salt.
      $newUser->setPassword(password_hash($values["password"], PASSWORD_BCRYPT));
      $newUser->save();

      return $this->createLoginResponse($response, $newUser);
   }

   public function logout($request, $response, $args) {

      $body = ['msg' => 'logout was called'];

      return HTTP::jsonResponse($response, $body);
   }

   private function createLoginResponse($response, $user) {
      $session = $this->createSession($user);
      $jwt_token = Auth::createJWTToken($session->getId(), $user->getId());

      $body = [];
      $body[Constants::JWT_ATTR] = $jwt_token;

      return HTTP::jsonResponse($response, $body);
   }

   private function createSession($user) {
      $token = base64_encode(mcrypt_create_iv(48, MCRYPT_DEV_URANDOM));
      $session = new Session();
      $session->setId($token);

      $current_time = time();

      $session->setUserId($user->getId());
      $session->setCreated($current_time);
      $session->setAccessed($current_time);
      $session->setExpire($current_time + Constants::TOKEN_EXPIRE_TIME);
      $session->save();

      return $session;
   }
}

?>
