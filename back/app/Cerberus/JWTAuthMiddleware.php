<?php
namespace Cerberus;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LogLevel;
use Monolog\Logger;

class JWTAuthMiddleware {
   // Indicates if SSL is required for authentication. Should be
   // set to 'true' for production.
   const HTTPS_REQUIRED       = "https_required";
   // Name of the cookie to look for the JWT token in.
   const COOKIE_NAME          = "cookie_name";
   // Attribute added to request with decoded JWT data
   const JWT_DATA_ATTRIBUTE   = "jwt_attr_data";
   // Callback function to be called during authentication for an
   // additional check.
   const CALLBACK             = "callback";
   // The JWT Secret.
   const SECRET               = "secret";

   protected $logger;

   protected $config = [
      self::HTTPS_REQUIRED      => true,
      self::COOKIE_NAME         => "token",
      self::JWT_DATA_ATTRIBUTE  => "jwt-data",
      self::CALLBACK            => null,
      self::SECRET              => "YouShouldNeverUseTheDefaultSecret"
   ];

   public function __construct(array $config = []) {
      if (!isset($config[self::SECRET])) {
         throw new Exception('The secret must be set in the config.');
      }

      foreach($config as $key => $value) {
         $this->config[$key] = $value;
      }
   }

   public function __invoke(RequestInterface $request, ResponseInterface $response, callable $next) {

      // Check for HTTPS Connection if required.
      if ($this->config[self::HTTPS_REQUIRED]) {
         $scheme = $request->getUri()->getScheme();
         if ("https" !== $scheme) {
            $this->log(LogLevel::ERROR, "Https is required for JWT Authentication");
            return $response->withStatus(401);
         }
      }

      // Find the JWT Token
      $jwtToken = $this->getJWTToken($request);
      if ($jwtToken == null) {
         $this->log(LogLevel::ERROR, "JWT Token could not be found.");
         return $response->withStatus(401);
      }

      // Attempt to decode the Token
      $decodedToken = JWTManager::decodeToken($jwtToken, $this->config[self::SECRET], $this->logger);
      if ($decodedToken == null) {
         $this->log(LogLevel::ERROR, "JWT Token could not be decoded");
         return $response->withStatus(401);
      }

      // Callback
      if (is_callable($this->config[self::CALLBACK])) {
         if (false == $this->config[self::CALLBACK]($request, $response, $decodedToken)) {
            $this->log(LogLevel::WARNING, "JWT Auth Callback returned false");
            return $response->withStatus(401);
         }
      }

      // Call Next
      return $next($request->withAttribute($this->config[self::JWT_DATA_ATTRIBUTE], $decodedToken), $response);
   }

   public function getJWTToken(RequestInterface $request) {
      // Try to get it from the request header
      $header = $request->getHeader("Authorization");
      if (isset($header[0])) {
         if (preg_match("/Bearer\s+(.*)$/i", $header[0], $matches)) {
            $this->log(LogLevel::DEBUG, "Found JWT Token in Authorization Header: " . $matches[1]);
            return $matches[1];
         }
      }

      // Can't find it in the Authorization Header
      $cookies = $request->getCookieParams();
      if (isset($cookies[$this->config[self::COOKIE_NAME]])) {
         $token = $cookies[$this->config[self::COOKIE_NAME]];
         $this->log(LogLevel::DEBUG, "Found JWT Token in Cookie: " . $token);
         return $token;
      }

      $this->log(LogLevel::WARNING, "Could not find the JWT Token");
      return null;
   }

   public function getLogger() {
      return $this->logger;
   }

   public function setLogger(Logger $logger) {
      $this->logger = $logger;
   }

   private function log($level, $message, array $context = []) {
      if ($this->logger) {
         return $this->logger->log($level, $message, $context);
      }
   }
}
?>
