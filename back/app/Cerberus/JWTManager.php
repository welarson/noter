<?php
namespace Cerberus;

use Firebase\JWT\JWT;
use Psr\Log\LogLevel;

class JWTManager {

   public static function createToken($token, $serverName, $expire, $secret, array $extra = []) {
      $currentTime = time();

      $data = [
         'iat'       => $currentTime,
         'jti'       => $token,
         'iss'       => $serverName,
         'nbf'       => $currentTime,
         'exp'       => $currentTime + $expire
      ];
      foreach( $extra as $key => $value) {
         $data[$key] = $value;
      }
      return JWT::encode($data, $secret, 'HS512');
   }

   public static function decodeToken($jwtToken, $secret, $logger = null) {
      try {
         return JWT::decode(
                $jwtToken,
                $secret,
                ["HS256", "HS512", "HS384", "RS256"]
            );
      } catch (\Exception $exception) {
         if ($logger !== null) {
            $logger->log(LogLevel::WARNING, $exception->getMessage(), [$jwtToken]);
         }
         return null;
      }
   }

   public static function getSecret() {
      //TODO: Get from environment.
      return "djo0AQW239uiknmd798slX9j3k24rijrsYdres";
   }
}
?>
