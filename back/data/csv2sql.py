#!/usr/bin/python3
import argparse
import csv
from datetime import datetime

def generateCategories(userQuery, filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['name']
            description = row['description']
            print("INSERT INTO category SET \n" +
                "    name='" + name + "',\n" +
                "    description='" + description + "',\n" +
                "    user_id=" + userQuery + ";\n")

def generateNotes(userQuery, filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            category = row['category']
            title = row['title']
            msg = row['msg']
            currentTime = datetime.now().strftime("%y-%m-%d %H:%M:%S")
            categoryQuery = "(SELECT id FROM category WHERE name='" + category + "')"
            print("INSERT INTO note SET \n" +
                "    user_id=" + userQuery + ",\n" +
                "    category_id=" + categoryQuery + ",\n" +
                "    created='" + currentTime + "',\n" +
                "    modified='" + currentTime + "',\n" +
                "    title='" + title + "',\n" +
                "    msg='" + msg + "';\n")

def generateUserSubQuery(user):
    sub_query = "(SELECT id FROM user WHERE email='" + user + "')"
    return sub_query;

parser = argparse.ArgumentParser()

parser.add_argument("-c", "--categories", help="Categories File")
parser.add_argument("-n", "--notes", help="Notes File")
parser.add_argument("-u", "--user", help="Email of user to add data to")

args = parser.parse_args()

sub_query = "(SELECT id FROM user WHERE email='" + args.user + "')"

generateCategories(sub_query, args.categories)
generateNotes(sub_query, args.notes)
