INSERT INTO category SET 
    name='Parks and Recreation',
    description='The absurd antics of an Indiana town''s public officials as they pursue sundry projects to make their city a better place.',
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com');

INSERT INTO category SET 
    name='30 Rock',
    description='Liz Lemon, head writer of the sketch comedy show "TGS with Tracy Jordan", must deal with an arrogant new boss and a crazy new star, all while trying to run a successful TV show without losing her mind. ',
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com');

INSERT INTO category SET 
    name='The Office',
    description='A mockumentary on a group of typical office workers, where the workday consists of ego clashes, inappropriate behavior, and tedium. Based on the hit BBC series. ',
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com');

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='The Office'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Michael Scott',
    msg='Don’t ever, for any reason, do anything to anyone, for any reason, ever, no matter what. No matter where. Or who, or who you are with, or where you are going, or where you’ve been, ever. For any reason, whatsoever.

Would I rather be feared or loved? Easy. Both. I want people to be afraid of how much they love me.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='The Office'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Dwight Schrute',
    msg='No, don’t call me a hero. Do you know who the real heroes are? The guys who wake up every morning and go into their normal jobs, and get a distress call from the Commissioner and take off their glasses and change into capes and fly around fighting crime. Those are the real heroes.

There are 3 things you never turn your back on: bears, men you have wronged, and a dominant male turkey during mating season.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='The Office'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Jim Halpert',
    msg='Everything I have I owe to this job...this stupid, wonderful, boring, amazing job.

Stanley just drank OJ out of my mug and didn''t seem to realize that it wasn''t his hot coffee. So the question has to be asked, is there no limit to what he won''t notice?';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='The Office'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Pam Beesly',
    msg='With work and two kids, nothing interesting''s gonna happen to us for a long, long time.

Michael''s been trying to get me and Jim to hang out with him ever since he started dating my mom. I don''t know. I really hoped this thing would just die out, but today he''s planning a birthday lunch for my mom and we have to go. No way out. No ... way ... out.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Leslie Knope',
    msg='I am big enough to admit I am often inspired by myself.

What I hear when I''m being yelled at is people caring loudly at me.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Ron Swanson',
    msg='When people get a little too chummy with me I like to call them by the wrong name to let them know I don’t really care about them.

It’s always a good idea to demonstrate to your coworkers that you are capable of withstanding a tremendous amount of pain.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='April Ludgate',
    msg='I declare that everything you are saying is stupid.

I think I may have found a project I''d actually enjoy doing, helping these cats and dogs. They should be rewarded for not being people. I hate people.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Andy Dwyer',
    msg='Anything is a toy if you play with it.

I have no idea what I''m doing, but I know I''m doing it really, really well.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Tom Haverford',
    msg='Whenever Leslie asks me for the Latin names of any of our plants, I just give her the names of rappers. Those are some Diddies. Those are some Bone Thugs-N-Harmoniums, right here. Those Ludacrises are coming in great.

Justin is hip. Pawnee is the opposite of hip. People in this town are just now getting into Nirvana. I don’t have the heart to tell them what’s gonna happen to Kurt Cobain in 1994.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='Parks and Recreation'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Ben Wyatt',
    msg='You know, “nerd culture” is mainstream now. So, when you use the word “nerd” derogatorily, it means you’re the one that’s out of the zeitgeist.

No, that’s Buckingham Palace. Hogwarts is fictional. You do know that, don’t you? It’s important to me that you know that.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='30 Rock'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Liz Lemon',
    msg='Why do you sound so surprised? I love America. Just because I think gay dudes should be allowed to adopt kids and we should all have hybrid cars doesn’t mean I don’t love America.

Why are my arms so weak? It’s like I did that push-up last year for nothing!';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='30 Rock'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Tracy Jordan',
    msg='Stop eatin'' people''s old french fries, pigeon. Have some self-respect. Don''t you know you can fly?

Nuh uh. Superman does good. You doin'' well. You need to study your grammar, son.';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='30 Rock'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Jack Donaghy',
    msg='Sure. I got you. New York third-wave feminist, college educated, single and pretending to be happy about it, over scheduled, undersexed, you buy any magazine that says "healthy body image" on the cover, and every two years you take up knitting for... a week.

No. I believe that when you have a problem you talk it over with your priest or your tailor or the mute elevator porter at your men''s club. Then you take that problem and crush it with your mind-vice. But for lesser being like curly-haired men and people who need glasses, therapy can help. And Lemon, I want you to get better. Because, and I mean this, I''m tired of talking this much to a woman I''m not having sex with. ';

INSERT INTO note SET 
    user_id=(SELECT id FROM user WHERE email='gunnar@dog.com'),
    category_id=(SELECT id FROM category WHERE name='30 Rock'),
    created='16-03-10 13:18:39',
    modified='16-03-10 13:18:39',
    title='Jenna Maroney',
    msg='Oh, no. Did I come across as interesting? ‘Cause I tried to mention Bono as much as possible.

I''m proud of you, Kenneth. You''ve got a good heart. I hope you get into a car accident some say so I can have it.';

