<?php

namespace NoterData;

use NoterData\Base\Logout as BaseLogout;

/**
 * Skeleton subclass for representing a row from the 'logout' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Logout extends BaseLogout
{

}
