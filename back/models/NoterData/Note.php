<?php

namespace NoterData;

use NoterData\Base\Note as BaseNote;

/**
 * Skeleton subclass for representing a row from the 'note' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Note extends BaseNote
{
   public function asMinimalArray() {
      return [
         "id" => $this->id,
         "category_id" =>$this->category_id,
         "created" => $this->created,
         "modified" => $this->modified,
         "title" => $this->title,
         "msg" => $this->msg
      ];
   }
}
