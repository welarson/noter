<?php

namespace NoterData;

use NoterData\Base\LogoutQuery as BaseLogoutQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'logout' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LogoutQuery extends BaseLogoutQuery
{

}
