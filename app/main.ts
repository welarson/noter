import {bootstrap}        from 'angular2/platform/browser';
import {provide}          from 'angular2/core';
import {FORM_PROVIDERS}   from 'angular2/common';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {HTTP_PROVIDERS}   from 'angular';

import {AppComponent} from './app.component';

bootstrap(
  AppComponent,
  [
    FORM_PROVIDERS,
    ROUTER_PROVIDERS,
    HTTP_PROVIDERS
  ]
);
